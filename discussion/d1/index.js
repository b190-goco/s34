/*
	npm init
	- prompt user for different settings that will define the application

	package.json
	- appears after 'npm init'
	- tracks version of our application

	npm install express
	

	npm install

	npm start
*/

const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.get("/", (req,res) => {
	res.send("Hello World");
});

app.get("/hello", (req,res) => {
	res.send("Hello from /hello endpoint");
})

app.post("/hello", (req,res) => {
	res.send(`Hello therem ${req.body.firstName} ${req.body.lastName}!`);
})

let users = [];

app.post("/signup", (req,res) =>{
	console.log(req.body);
/*
	create an if-else statement stating that:
		- if fn and ln are not "", the object will be pushed in the users array and send succesful sign up
		- else fn and ln should be filled
*/	
	if (req.body.firstName!="" && req.body.lastName!="") {
		users.push({
			firstName: req.body.firstName,
			lastName: req.body.lastName
		});
		res.send("Sign Up Succesful!");
	}
	else {
		res.send("Fill the required fields.");
	}
	console.log(users);
})

app.put("/change-lastName", (req,res) => {
	let message;

	for (let i=0;i<users.length;i++){
		if (req.body.firstName==users[i].firstName){
			users[i].lastName = req.body.lastName;

			message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`;
			console.log(users);
			break;
		}
		else {
			message = "user does not exist";
		}
	}
	res.send(message);
})

// ================================================================================================ //

/* ACTIVITY*/

// 1. Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home", (req,res) => {
	res.send("Welcome to the homepage!");
});
// 2. Process a GET request at the "/home" route using postman.
// go to http://localhost:3000/home
users = [
	{
		username: "johndoe",
		password: "johndoe1234"
	}
];
// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.get("/users", (req,res) => {
	res.send(users);
});
// 4. Process a GET request at the "/users" route using postman.
// go to http://localhost:3000/users

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-user", (req,res) => {
	for (let i=0;i<users.length;i++){
		if (req.body.username==users[i].username){
			var deletedUser = users.splice(i,1);
			message = `User ${req.body.username} has been deleted.`;
			//console.log(i);
			//console.log(users);
			// console.log(users.splice(0,0));
			//console.log(message);
			console.log(deletedUser);
		}
		else {
			message = "user does not exist";
			break;
		}
		res.send(message)
	}
});
// 

// 6. Process a DELETE request at the "/delete-user" route using postman.
// go to http://localhost:3000/delete-user

// 7. Export the Postman collection and save it inside the root folder of our application.
// 8. Create a git repository named S34.
// 9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 10. Add the link in Boodle.

app.listen(port, () => console.log(`Server running at port: ${port}`));